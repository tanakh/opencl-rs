import Data.Char
import Control.Monad
import Text.Printf
import Data.Maybe
import Data.List

ext_const :: String -> Maybe (String, [String])
ext_const s =
    case words s of
        ("#define": name: _) ->
            Just (name, words $ map (\c -> if c == '_' then ' ' else c) name)
        _ ->
            Nothing

capitalize (c:cs) = toUpper c : map toLower cs

make_enum prefix = do
    ls <- lines <$> getContents
    let cs = mapMaybe ext_const (ls :: [String])

    putStrLn "enum Hoge {"
    forM_ cs $ \(name, ("CL": ps)) -> do
        let pss = if head ps == prefix then tail ps else ps
        printf "  %s = %s as isize,\n" (concatMap capitalize pss) name
    putStrLn "}"
    
-- main = make_enum "DEVICE"

split :: Int -> [a] -> [[a]]
split n [] = []
split n ls = take n ls : split n (drop n ls)

sep separator ss = words $ map (\c -> if c == separator then ' ' else c) ss

typeMap = [
    ("cl_uint", "u32"),
    ("cl_ulong", "u64"),
    ("size_t", "usize"),
    ("cl_bool", "bool"),

    ("cl_platform_id", "Platform"),
    ("cl_device_id", "Device"),

    ("***", "***")
    ]

findType s
    | s == "char[]" = "String"
    | "[]" `isSuffixOf` s =
        let t = findType $ reverse $ drop 2 $ reverse s
        in "Vec<" ++ t ++ ">"
    | Just ty <- lookup s typeMap =
        ty
    -- | "cl_device_" `isPrefixOf` s =
    --     concatMap capitalize $ drop 2 $ sep '_' s

    | "cl_" `isPrefixOf` s =
        concatMap capitalize $ drop 1 $ sep '_' s
            
    | otherwise = error $ "type `" ++ s ++ "` cannot found in table"

sanitize s
    | s == "type" = "get_type"
    | otherwise = s

genInfoMethods prefix = do
    es <- split 2 . words <$> getContents
    forM_ es $ \[name, ty] -> do
        let rty = findType ty
        let ps = sep '_' name :: [String]
        let pss = case ps of
                    ("CL": s: ss) 
                        | s == prefix -> ss
                        | otherwise -> s:ss
        printf "    info_method!(%s, %s, %s);\n" (sanitize $ map toLower$ intercalate "_" pss :: String) (name :: String) (rty :: String)

genInfoTests prefix = do
    ls <- lines <$> getContents
    let cs = mapMaybe ext_const (ls :: [String])
    let maxlen = maximum $ map (\(name, _) -> length name) cs

    forM_ cs $ \(name, ("CL": ps)) -> do
        let pss = if head ps == prefix then tail ps else ps
        printf "    println!(\"  %s: {:?}\", device.%s());\n" (take maxlen $ name ++ replicate maxlen ' ') (sanitize $ map toLower$ intercalate "_" pss :: String)
    
main = genInfoTests "DEVICE"

