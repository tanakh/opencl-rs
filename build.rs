extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rustc-link-lib=OpenCL");

    let bindings = bindgen::Builder::default()
        .clang_arg("-IOpenCL-Headers/opencl22")
        .header("OpenCL-Headers/opencl22/CL/opencl.h")
        .whitelist_function("cl.*")
        .whitelist_type("cl_.*")
        .whitelist_var("CL_.*")
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings-opencl.rs"))
        .expect("Couldn't write bindings!");
}
