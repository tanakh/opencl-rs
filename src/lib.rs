#[macro_use]
extern crate bitflags;
#[macro_use]
extern crate error_chain;
extern crate libc;
extern crate num;
#[macro_use]
extern crate num_derive;

pub mod raw;

use num::FromPrimitive;
use raw::*;
use std::ffi::{CStr, CString};
use std::ptr::{null, null_mut};
use std::os::raw::c_void;
use std::time::Duration;
use libc::size_t;

error_chain!{
    foreign_links {
        NulError(std::ffi::NulError);
    }
    errors {
        CLError(code: ErrorCode) {
            description("OpenCL Error")
            display("OpenCL Error: {:?}", code)
        }

        InvalidParameter(v: String) {
            description("Invalid parameter")
            display("Invalid parameter: {}", v)
        }
    }
}

#[derive(Copy, Clone, Debug, FromPrimitive)]
pub enum ErrorCode {
    Success = CL_SUCCESS as isize,
    DeviceNotFound = CL_DEVICE_NOT_FOUND as isize,
    DeviceNotAvailable = CL_DEVICE_NOT_AVAILABLE as isize,
    CompilerNotAvailable = CL_COMPILER_NOT_AVAILABLE as isize,
    MemObjectAllocationFailure = CL_MEM_OBJECT_ALLOCATION_FAILURE as isize,
    OutOfResources = CL_OUT_OF_RESOURCES as isize,
    OutOfHostMemory = CL_OUT_OF_HOST_MEMORY as isize,
    ProfilingInfoNotAvailable = CL_PROFILING_INFO_NOT_AVAILABLE as isize,
    MemCopyOverlap = CL_MEM_COPY_OVERLAP as isize,
    ImageFormatMismatch = CL_IMAGE_FORMAT_MISMATCH as isize,
    ImageFormatNotSupported = CL_IMAGE_FORMAT_NOT_SUPPORTED as isize,
    BuildProgramFailure = CL_BUILD_PROGRAM_FAILURE as isize,
    MapFailure = CL_MAP_FAILURE as isize,
    MisalignedSubBufferOffset = CL_MISALIGNED_SUB_BUFFER_OFFSET as isize,
    ExecStatusErrorForEventsInWaitList = CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST as isize,
    CompileProgramFailure = CL_COMPILE_PROGRAM_FAILURE as isize,
    LinkerNotAvailable = CL_LINKER_NOT_AVAILABLE as isize,
    LinkProgramFailure = CL_LINK_PROGRAM_FAILURE as isize,
    DevicePartitionFailed = CL_DEVICE_PARTITION_FAILED as isize,
    KernelArgInfoNotAvailable = CL_KERNEL_ARG_INFO_NOT_AVAILABLE as isize,

    InvalidValue = CL_INVALID_VALUE as isize,
    InvalidDeviceType = CL_INVALID_DEVICE_TYPE as isize,
    InvalidPlatform = CL_INVALID_PLATFORM as isize,
    InvalidDevice = CL_INVALID_DEVICE as isize,
    InvalidContext = CL_INVALID_CONTEXT as isize,
    InvalidQueueProperties = CL_INVALID_QUEUE_PROPERTIES as isize,
    InvalidCommandQueue = CL_INVALID_COMMAND_QUEUE as isize,
    InvalidHostPtr = CL_INVALID_HOST_PTR as isize,
    InvalidMemObject = CL_INVALID_MEM_OBJECT as isize,
    InvalidImageFormatDescriptor = CL_INVALID_IMAGE_FORMAT_DESCRIPTOR as isize,
    InvalidImageSize = CL_INVALID_IMAGE_SIZE as isize,
    InvalidSampler = CL_INVALID_SAMPLER as isize,
    InvalidBinary = CL_INVALID_BINARY as isize,
    InvalidBuildOptions = CL_INVALID_BUILD_OPTIONS as isize,
    InvalidProgram = CL_INVALID_PROGRAM as isize,
    InvalidProgramExecutable = CL_INVALID_PROGRAM_EXECUTABLE as isize,
    InvalidKernelName = CL_INVALID_KERNEL_NAME as isize,
    InvalidKernelDefinition = CL_INVALID_KERNEL_DEFINITION as isize,
    InvalidKernel = CL_INVALID_KERNEL as isize,
    InvalidArgIndex = CL_INVALID_ARG_INDEX as isize,
    InvalidArgValue = CL_INVALID_ARG_VALUE as isize,
    InvalidArgSize = CL_INVALID_ARG_SIZE as isize,
    InvalidKernelArgs = CL_INVALID_KERNEL_ARGS as isize,
    InvalidWorkDimension = CL_INVALID_WORK_DIMENSION as isize,
    InvalidWorkGroupSize = CL_INVALID_WORK_GROUP_SIZE as isize,
    InvalidWorkItemSize = CL_INVALID_WORK_ITEM_SIZE as isize,
    InvalidGlobalOffset = CL_INVALID_GLOBAL_OFFSET as isize,
    InvalidEventWaitList = CL_INVALID_EVENT_WAIT_LIST as isize,
    InvalidEvent = CL_INVALID_EVENT as isize,
    InvalidOperation = CL_INVALID_OPERATION as isize,
    InvalidGlObject = CL_INVALID_GL_OBJECT as isize,
    InvalidBufferSize = CL_INVALID_BUFFER_SIZE as isize,
    InvalidMipLevel = CL_INVALID_MIP_LEVEL as isize,
    InvalidGlobalWorkSize = CL_INVALID_GLOBAL_WORK_SIZE as isize,
    InvalidProperty = CL_INVALID_PROPERTY as isize,
    InvalidImageDescriptor = CL_INVALID_IMAGE_DESCRIPTOR as isize,
    InvalidCompilerOptions = CL_INVALID_COMPILER_OPTIONS as isize,
    InvalidLinkerOptions = CL_INVALID_LINKER_OPTIONS as isize,
    InvalidDevicePartitionCount = CL_INVALID_DEVICE_PARTITION_COUNT as isize,
    InvalidPipeSize = CL_INVALID_PIPE_SIZE as isize,
    InvalidDeviceQueue = CL_INVALID_DEVICE_QUEUE as isize,
    InvalidSpecId = CL_INVALID_SPEC_ID as isize,
    MaxSizeRestrictionExceeded = CL_MAX_SIZE_RESTRICTION_EXCEEDED as isize,
}

fn to_clerror(ret: cl_int) -> Result<()> {
    if ret != CL_SUCCESS as cl_int {
        bail!(ErrorKind::CLError(ErrorCode::from_i32(ret).unwrap()));
    }
    Ok(())
}

macro_rules! check {
    ($e: expr) => {
        to_clerror(unsafe { $e })?
    };
}

type InfoFn<T> = unsafe extern "C" fn(T, cl_uint, usize, *mut c_void, *mut usize) -> i32;

pub trait Object {
    type InnerType;
    fn inner(&self) -> Self::InnerType;

    const GET_FN: InfoFn<Self::InnerType>;

    fn get_info<T: InfoType>(&self, name: cl_uint) -> Result<T> {
        T::get_info(|a, b, c| unsafe { Self::GET_FN(self.inner(), name, a, b, c) })
    }
}

pub trait InfoType: Sized {
    type RawType;
    fn new(size: usize) -> Self::RawType {
        assert_eq!(size, std::mem::size_of::<Self::RawType>());
        unsafe { std::mem::zeroed() }
    }
    fn as_mut_ptr(val: &mut Self::RawType) -> *mut c_void {
        val as *mut _ as _
    }
    fn from_raw(val: Self::RawType) -> Self;

    fn get_info<F: Fn(usize, *mut c_void, *mut usize) -> cl_int>(f: F) -> Result<Self> {
        let mut size = 0;
        to_clerror(f(0, null_mut(), &mut size))?;
        let mut ret = Self::new(size);
        to_clerror(f(size, Self::as_mut_ptr(&mut ret), null_mut()))?;
        Ok(Self::from_raw(ret))
    }
}

impl InfoType for String {
    type RawType = Vec<u8>;
    fn new(size: usize) -> Self::RawType {
        vec![0u8; size]
    }
    fn as_mut_ptr(val: &mut Self::RawType) -> *mut c_void {
        val.as_mut_ptr() as _
    }
    fn from_raw(val: Self::RawType) -> Self {
        CStr::from_bytes_with_nul(&val)
            .unwrap()
            .to_string_lossy()
            .into_owned()
    }
}

impl<T: InfoType> InfoType for Vec<T>
where
    T::RawType: Clone,
{
    type RawType = Vec<T::RawType>;
    fn new(size: usize) -> Self::RawType {
        let elem_size = std::mem::size_of::<T>();
        assert_eq!(size % elem_size, 0);
        vec![T::new(elem_size); size / elem_size]
    }
    fn as_mut_ptr(val: &mut Self::RawType) -> *mut c_void {
        val.as_mut_ptr() as _
    }
    fn from_raw(val: Self::RawType) -> Self {
        val.into_iter().map(T::from_raw).collect()
    }
}

impl<T, U> InfoType for Option<T>
where
    T: InfoType<RawType = *mut U>,
{
    type RawType = *mut ();
    fn new(size: usize) -> Self::RawType {
        assert_eq!(size, std::mem::size_of::<Self::RawType>());
        null_mut()
    }
    fn as_mut_ptr(val: &mut Self::RawType) -> *mut c_void {
        val as *mut Self::RawType as _
    }
    fn from_raw(val: Self::RawType) -> Self {
        if val.is_null() {
            None
        } else {
            Some(T::from_raw(val as T::RawType))
        }
    }
}

impl<T> InfoType for *mut T {
    type RawType = *mut T;
    fn new(size: usize) -> Self::RawType {
        assert_eq!(size, std::mem::size_of::<Self::RawType>());
        null_mut()
    }
    fn as_mut_ptr(val: &mut Self::RawType) -> *mut c_void {
        val as *mut Self::RawType as _
    }
    fn from_raw(val: Self::RawType) -> Self {
        val
    }
}

macro_rules! impl_info_type {
    ($rust_ty: ty, $cl_ty: ty) => {
        impl InfoType for $rust_ty {
            type RawType = $cl_ty;
            fn from_raw(val: Self::RawType) -> Self {
                val as Self
            }
        }
    };
}

macro_rules! impl_info_type_enum {
    ($rust_ty: ty, $cl_ty: ty) => {
        impl InfoType for $rust_ty {
            type RawType = $cl_ty;
            fn from_raw(val: Self::RawType) -> Self {
                FromPrimitive::from_i64(val as i64).unwrap()
            }
        }
    };
}

macro_rules! impl_info_type_bitfield {
    ($rust_ty: ty) => {
        impl InfoType for $rust_ty {
            type RawType = cl_bitfield;
            fn from_raw(val: Self::RawType) -> Self {
                Self::from_bits(val).unwrap()
            }
        }
    };
}

impl_info_type!(usize, size_t);
impl_info_type!(u32, cl_uint);
impl_info_type!(u64, cl_ulong);

impl InfoType for bool {
    type RawType = cl_bool;
    fn from_raw(val: Self::RawType) -> Self {
        val != 0
    }
}

macro_rules! info_method {
    ($method:ident, $name:expr, $ret:ty) => {
        pub fn $method(&self) -> Result<$ret> {
            self.get_info($name)
        }
    }
}

fn get_objects<T, F: Fn(cl_uint, *mut *mut T, *mut cl_uint) -> cl_int>(
    f: F,
) -> Result<Vec<*mut T>> {
    let mut num = 0;
    to_clerror(f(0, null_mut(), &mut num))?;
    let mut ret = vec![null_mut(); num as usize];
    to_clerror(f(num, ret.as_mut_ptr() as _, null_mut()))?;
    Ok(ret)
}

fn to_inner_vec<T: Object>(objs: &[&T]) -> Vec<T::InnerType> {
    objs.iter().map(|e| e.inner()).collect()
}

fn str_to_cstring(s: &str) -> Result<CString> {
    Ok(CString::new(s.as_bytes().to_owned())?)
}

#[derive(Debug)]
pub struct Platform(raw::cl_platform_id);

impl Object for Platform {
    type InnerType = cl_platform_id;
    fn inner(&self) -> Self::InnerType {
        self.0
    }
    const GET_FN: InfoFn<Self::InnerType> = clGetPlatformInfo;
}

impl InfoType for Platform {
    type RawType = cl_platform_id;
    fn from_raw(val: Self::RawType) -> Self {
        Platform(val)
    }
}

impl Platform {
    pub fn get() -> Result<Vec<Platform>> {
        Ok(get_objects(|n, p, q| unsafe { clGetPlatformIDs(n, p, q) })?
            .into_iter()
            .map(Platform)
            .collect())
    }

    pub fn first() -> Result<Option<Platform>> {
        let mut num = 0;
        let mut platform = null_mut();
        check!(raw::clGetPlatformIDs(1, &mut platform, &mut num));
        Ok(if num == 0 {
            None
        } else {
            Some(Platform(platform))
        })
    }

    pub fn devices(&self, device_type: DeviceType) -> Result<Vec<Device>> {
        Ok(get_objects(|n, p, q| unsafe {
            clGetDeviceIDs(self.inner(), device_type as cl_device_type, n, p, q)
        })?.into_iter()
            .map(Device)
            .collect())
    }

    pub fn unload_compiler(&self) -> Result<()> {
        check!(clUnloadPlatformCompiler(self.inner()));
        Ok(())
    }

    pub fn extension_function_address(&self, func_name: &str) -> *const () {
        unsafe {
            clGetExtensionFunctionAddressForPlatform(
                self.inner(),
                str_to_cstring(func_name).unwrap().as_ptr(),
            ) as _
        }
    }

    info_method!(profile, CL_PLATFORM_PROFILE, String);
    info_method!(version, CL_PLATFORM_VERSION, String);
    info_method!(name, CL_PLATFORM_NAME, String);
    info_method!(vendor, CL_PLATFORM_VENDOR, String);
    info_method!(extensions, CL_PLATFORM_EXTENSIONS, String);
    info_method!(
        host_timer_resolution,
        CL_PLATFORM_HOST_TIMER_RESOLUTION,
        usize
    );
}

#[derive(Clone, Copy, Debug, FromPrimitive)]
pub enum DeviceType {
    Default = CL_DEVICE_TYPE_DEFAULT as isize,
    CPU = CL_DEVICE_TYPE_CPU as isize,
    GPU = CL_DEVICE_TYPE_GPU as isize,
    Accelarator = CL_DEVICE_TYPE_ACCELERATOR as isize,
    Custom = CL_DEVICE_TYPE_CUSTOM as isize,
    All = CL_DEVICE_TYPE_ALL as isize,
}
impl_info_type_enum!(DeviceType, cl_device_type);

#[derive(Clone, Copy, Debug)]
pub enum DeviceInfo {
    Type = CL_DEVICE_TYPE as isize,
    VendorId = CL_DEVICE_VENDOR_ID as isize,
    MaxComputeUnits = CL_DEVICE_MAX_COMPUTE_UNITS as isize,
    MaxWorkItemDimensions = CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS as isize,
    MaxWorkGroupSize = CL_DEVICE_MAX_WORK_GROUP_SIZE as isize,
    MaxWorkItemSizes = CL_DEVICE_MAX_WORK_ITEM_SIZES as isize,
    PreferredVectorWidthChar = CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR as isize,
    PreferredVectorWidthShort = CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT as isize,
    PreferredVectorWidthInt = CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT as isize,
    PreferredVectorWidthLong = CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG as isize,
    PreferredVectorWidthFloat = CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT as isize,
    PreferredVectorWidthDouble = CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE as isize,
    MaxClockFrequency = CL_DEVICE_MAX_CLOCK_FREQUENCY as isize,
    AddressBits = CL_DEVICE_ADDRESS_BITS as isize,
    MaxReadImageArgs = CL_DEVICE_MAX_READ_IMAGE_ARGS as isize,
    MaxWriteImageArgs = CL_DEVICE_MAX_WRITE_IMAGE_ARGS as isize,
    MaxMemAllocSize = CL_DEVICE_MAX_MEM_ALLOC_SIZE as isize,
    Image2dMaxWidth = CL_DEVICE_IMAGE2D_MAX_WIDTH as isize,
    Image2dMaxHeight = CL_DEVICE_IMAGE2D_MAX_HEIGHT as isize,
    Image3dMaxWidth = CL_DEVICE_IMAGE3D_MAX_WIDTH as isize,
    Image3dMaxHeight = CL_DEVICE_IMAGE3D_MAX_HEIGHT as isize,
    Image3dMaxDepth = CL_DEVICE_IMAGE3D_MAX_DEPTH as isize,
    ImageSupport = CL_DEVICE_IMAGE_SUPPORT as isize,
    MaxParameterSize = CL_DEVICE_MAX_PARAMETER_SIZE as isize,
    MaxSamplers = CL_DEVICE_MAX_SAMPLERS as isize,
    MemBaseAddrAlign = CL_DEVICE_MEM_BASE_ADDR_ALIGN as isize,
    MinDataTypeAlignSize = CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE as isize,
    SingleFpConfig = CL_DEVICE_SINGLE_FP_CONFIG as isize,
    GlobalMemCacheType = CL_DEVICE_GLOBAL_MEM_CACHE_TYPE as isize,
    GlobalMemCachelineSize = CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE as isize,
    GlobalMemCacheSize = CL_DEVICE_GLOBAL_MEM_CACHE_SIZE as isize,
    GlobalMemSize = CL_DEVICE_GLOBAL_MEM_SIZE as isize,
    MaxConstantBufferSize = CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE as isize,
    MaxConstantArgs = CL_DEVICE_MAX_CONSTANT_ARGS as isize,
    LocalMemType = CL_DEVICE_LOCAL_MEM_TYPE as isize,
    LocalMemSize = CL_DEVICE_LOCAL_MEM_SIZE as isize,
    ErrorCorrectionSupport = CL_DEVICE_ERROR_CORRECTION_SUPPORT as isize,
    ProfilingTimerResolution = CL_DEVICE_PROFILING_TIMER_RESOLUTION as isize,
    EndianLittle = CL_DEVICE_ENDIAN_LITTLE as isize,
    Available = CL_DEVICE_AVAILABLE as isize,
    CompilerAvailable = CL_DEVICE_COMPILER_AVAILABLE as isize,
    ExecutionCapabilities = CL_DEVICE_EXECUTION_CAPABILITIES as isize,
    // QueueProperties = CL_DEVICE_QUEUE_PROPERTIES as isize, /* deprecated */
    QueueOnHostProperties = CL_DEVICE_QUEUE_ON_HOST_PROPERTIES as isize,
    Name = CL_DEVICE_NAME as isize,
    Vendor = CL_DEVICE_VENDOR as isize,
    DriverVersion = CL_DRIVER_VERSION as isize,
    Profile = CL_DEVICE_PROFILE as isize,
    Version = CL_DEVICE_VERSION as isize,
    Extensions = CL_DEVICE_EXTENSIONS as isize,
    Platform = CL_DEVICE_PLATFORM as isize,
    DoubleFpConfig = CL_DEVICE_DOUBLE_FP_CONFIG as isize,
    HalfFpConfig = CL_DEVICE_HALF_FP_CONFIG as isize,
    PreferredVectorWidthHalf = CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF as isize,
    // HostUnifiedMemory = CL_DEVICE_HOST_UNIFIED_MEMORY as isize, /* deprecated */
    NativeVectorWidthChar = CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR as isize,
    NativeVectorWidthShort = CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT as isize,
    NativeVectorWidthInt = CL_DEVICE_NATIVE_VECTOR_WIDTH_INT as isize,
    NativeVectorWidthLong = CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG as isize,
    NativeVectorWidthFloat = CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT as isize,
    NativeVectorWidthDouble = CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE as isize,
    NativeVectorWidthHalf = CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF as isize,
    OpenclCVersion = CL_DEVICE_OPENCL_C_VERSION as isize,
    LinkerAvailable = CL_DEVICE_LINKER_AVAILABLE as isize,
    BuiltInKernels = CL_DEVICE_BUILT_IN_KERNELS as isize,
    ImageMaxBufferSize = CL_DEVICE_IMAGE_MAX_BUFFER_SIZE as isize,
    ImageMaxArraySize = CL_DEVICE_IMAGE_MAX_ARRAY_SIZE as isize,
    ParentDevice = CL_DEVICE_PARENT_DEVICE as isize,
    PartitionMaxSubDevices = CL_DEVICE_PARTITION_MAX_SUB_DEVICES as isize,
    PartitionProperties = CL_DEVICE_PARTITION_PROPERTIES as isize,
    PartitionAffinityDomain = CL_DEVICE_PARTITION_AFFINITY_DOMAIN as isize,
    PartitionType = CL_DEVICE_PARTITION_TYPE as isize,
    ReferenceCount = CL_DEVICE_REFERENCE_COUNT as isize,
    PreferredInteropUserSync = CL_DEVICE_PREFERRED_INTEROP_USER_SYNC as isize,
    PrintfBufferSize = CL_DEVICE_PRINTF_BUFFER_SIZE as isize,
    ImagePitchAlignment = CL_DEVICE_IMAGE_PITCH_ALIGNMENT as isize,
    ImageBaseAddressAlignment = CL_DEVICE_IMAGE_BASE_ADDRESS_ALIGNMENT as isize,
    MaxReadWriteImageArgs = CL_DEVICE_MAX_READ_WRITE_IMAGE_ARGS as isize,
    MaxGlobalVariableSize = CL_DEVICE_MAX_GLOBAL_VARIABLE_SIZE as isize,
    QueueOnDeviceProperties = CL_DEVICE_QUEUE_ON_DEVICE_PROPERTIES as isize,
    QueueOnDevicePreferredSize = CL_DEVICE_QUEUE_ON_DEVICE_PREFERRED_SIZE as isize,
    QueueOnDeviceMaxSize = CL_DEVICE_QUEUE_ON_DEVICE_MAX_SIZE as isize,
    MaxOnDeviceQueues = CL_DEVICE_MAX_ON_DEVICE_QUEUES as isize,
    MaxOnDeviceEvents = CL_DEVICE_MAX_ON_DEVICE_EVENTS as isize,
    SvmCapabilities = CL_DEVICE_SVM_CAPABILITIES as isize,
    GlobalVariablePreferredTotalSize = CL_DEVICE_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE as isize,
    MaxPipeArgs = CL_DEVICE_MAX_PIPE_ARGS as isize,
    PipeMaxActiveReservations = CL_DEVICE_PIPE_MAX_ACTIVE_RESERVATIONS as isize,
    PipeMaxPacketSize = CL_DEVICE_PIPE_MAX_PACKET_SIZE as isize,
    PreferredPlatformAtomicAlignment = CL_DEVICE_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT as isize,
    PreferredGlobalAtomicAlignment = CL_DEVICE_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT as isize,
    PreferredLocalAtomicAlignment = CL_DEVICE_PREFERRED_LOCAL_ATOMIC_ALIGNMENT as isize,
    IlVersion = CL_DEVICE_IL_VERSION as isize,
    MaxNumSubGroups = CL_DEVICE_MAX_NUM_SUB_GROUPS as isize,
    SubGroupIndependentForwardProgress = CL_DEVICE_SUB_GROUP_INDEPENDENT_FORWARD_PROGRESS as isize,
}

#[derive(Debug)]
pub struct Device(raw::cl_device_id);

impl Object for Device {
    type InnerType = cl_device_id;
    fn inner(&self) -> Self::InnerType {
        self.0
    }
    const GET_FN: InfoFn<Self::InnerType> = clGetDeviceInfo;
}

impl InfoType for Device {
    type RawType = cl_device_id;
    fn from_raw(val: Self::RawType) -> Self {
        Device(val)
    }
}

bitflags! {
    pub struct DeviceExecCapabilities: cl_bitfield {
        const KERNEL = CL_EXEC_KERNEL as cl_bitfield;
        const NATIVE_KERNEL = CL_EXEC_NATIVE_KERNEL as cl_bitfield;
    }
}
impl_info_type_bitfield!(DeviceExecCapabilities);

bitflags! {
    pub struct DeviceFpConfig: cl_bitfield {
        const DENORM = CL_FP_DENORM as cl_bitfield;
        const INF_NAN = CL_FP_INF_NAN as cl_bitfield;
        const ROUND_TO_NEAREST = CL_FP_ROUND_TO_NEAREST as cl_bitfield;
        const ROUND_TO_ZERO = CL_FP_ROUND_TO_ZERO as cl_bitfield;
        const ROUND_TO_INF = CL_FP_ROUND_TO_INF as cl_bitfield;
        const FMA = CL_FP_FMA as cl_bitfield;
        const SOFT_FLOAT = CL_FP_SOFT_FLOAT as cl_bitfield;
        const CORRECTLY_ROUNDED_DIVIDE_SQRT = CL_FP_CORRECTLY_ROUNDED_DIVIDE_SQRT as cl_bitfield;
    }
}
impl_info_type_bitfield!(DeviceFpConfig);

#[derive(Clone, Copy, Debug, FromPrimitive)]
pub enum DeviceMemCacheType {
    None = CL_NONE as isize,
    ReadOnlyCache = CL_READ_ONLY_CACHE as isize,
    ReadWriteCache = CL_READ_WRITE_CACHE as isize,
}
impl_info_type_enum!(DeviceMemCacheType, cl_device_mem_cache_type);

#[derive(Clone, Copy, Debug, FromPrimitive)]
pub enum DeviceLocalMemType {
    Local = CL_LOCAL as isize,
    Global = CL_GLOBAL as isize,
}
impl_info_type_enum!(DeviceLocalMemType, cl_device_local_mem_type);

bitflags! {
    pub struct CommandQueueProperties: cl_bitfield {
        const OUT_OF_ORDER_EXEC_MODE_ENABLE = CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE as cl_bitfield;
        const PROFILING_ENABLE = CL_QUEUE_PROFILING_ENABLE as cl_bitfield;
        const ON_DEVICE = CL_QUEUE_ON_DEVICE as cl_bitfield;
        const ON_DEVICE_DEFAULT = CL_QUEUE_ON_DEVICE_DEFAULT as cl_bitfield;
    }
}
impl_info_type_bitfield!(CommandQueueProperties);

bitflags! {
    pub struct DeviceAffinityDomain: cl_bitfield {
        const NUMA = CL_DEVICE_AFFINITY_DOMAIN_NUMA as cl_bitfield;
        const L4_CACHE = CL_DEVICE_AFFINITY_DOMAIN_L4_CACHE as cl_bitfield;
        const L3_CACHE = CL_DEVICE_AFFINITY_DOMAIN_L3_CACHE as cl_bitfield;
        const L2_CACHE = CL_DEVICE_AFFINITY_DOMAIN_L2_CACHE as cl_bitfield;
        const L1_CACHE = CL_DEVICE_AFFINITY_DOMAIN_L1_CACHE as cl_bitfield;
        const NEXT_PARTITIONABLE = CL_DEVICE_AFFINITY_DOMAIN_NEXT_PARTITIONABLE as cl_bitfield;
    }
}
impl_info_type_bitfield!(DeviceAffinityDomain);

bitflags! {
    pub struct DeviceSvmCapabilities: cl_bitfield {
        const COARSE_GRAIN_BUFFER = CL_DEVICE_SVM_COARSE_GRAIN_BUFFER as cl_bitfield;
        const FINE_GRAIN_BUFFER = CL_DEVICE_SVM_FINE_GRAIN_BUFFER as cl_bitfield;
        const FINE_GRAIN_SYSTEM = CL_DEVICE_SVM_FINE_GRAIN_SYSTEM as cl_bitfield;
        const ATOMICS = CL_DEVICE_SVM_ATOMICS as cl_bitfield;
    }
}
impl_info_type_bitfield!(DeviceSvmCapabilities);

#[cfg_attr(rustfmt, rustfmt_skip)]
impl Device {
    // create_sub_devices()
    // default()
    // set_default()

    pub fn device_and_host_timer(&self) -> Result<(Duration, Duration)> {
        let mut device_timestamp = 0;
        let mut host_timestamp = 0;
        check!(clGetDeviceAndHostTimer(self.inner(), &mut device_timestamp, &mut host_timestamp));
        Ok((Duration::new(device_timestamp / 1_000_000_000, (device_timestamp % 1_000_000_000) as u32),
            Duration::new(host_timestamp / 1_000_000_000, (host_timestamp % 1_000_000_000) as u32)))
    }

    pub fn host_timer(&self) -> Result<Duration> {
        let mut host_timestamp = 0;
        check!(clGetHostTimer(self.inner(), &mut host_timestamp));
        Ok(Duration::new(host_timestamp / 1_000_000_000, (host_timestamp % 1_000_000_000) as u32))
    }

    info_method!(address_bits, CL_DEVICE_ADDRESS_BITS, u32);
    info_method!(available, CL_DEVICE_AVAILABLE, bool);
    info_method!(built_in_kernels, CL_DEVICE_BUILT_IN_KERNELS, String);
    info_method!(compiler_available, CL_DEVICE_COMPILER_AVAILABLE, bool);
    info_method!(double_fp_config, CL_DEVICE_DOUBLE_FP_CONFIG, DeviceFpConfig);
    info_method!(endian_little, CL_DEVICE_ENDIAN_LITTLE, bool);
    info_method!(error_correction_support, CL_DEVICE_ERROR_CORRECTION_SUPPORT, bool);
    info_method!(execution_capabilities, CL_DEVICE_EXECUTION_CAPABILITIES, DeviceExecCapabilities);
    info_method!(extensions, CL_DEVICE_EXTENSIONS, String);
    info_method!(global_mem_cache_size, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, u64);
    info_method!(global_mem_cache_type, CL_DEVICE_GLOBAL_MEM_CACHE_TYPE, DeviceMemCacheType);
    info_method!(global_mem_cacheline_size, CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, u32);
    info_method!(global_mem_size, CL_DEVICE_GLOBAL_MEM_SIZE, u64);
    info_method!(global_variable_preferred_total_size, CL_DEVICE_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE, usize);
    // info_method!(half_fp_config, CL_DEVICE_HALF_FP_CONFIG, DeviceFpConfig); // FIXME: this is removed in recent version?
    info_method!(il_version, CL_DEVICE_IL_VERSION, String);
    info_method!(image2d_max_height, CL_DEVICE_IMAGE2D_MAX_HEIGHT, usize);
    info_method!(image2d_max_width, CL_DEVICE_IMAGE2D_MAX_WIDTH, usize);
    info_method!(image3d_max_depth, CL_DEVICE_IMAGE3D_MAX_DEPTH, usize);
    info_method!(image3d_max_height, CL_DEVICE_IMAGE3D_MAX_HEIGHT, usize);
    info_method!(image3d_max_width, CL_DEVICE_IMAGE3D_MAX_WIDTH, usize);
    info_method!(image_base_address_alignment, CL_DEVICE_IMAGE_BASE_ADDRESS_ALIGNMENT, u32);
    info_method!(image_max_array_size, CL_DEVICE_IMAGE_MAX_ARRAY_SIZE, usize);
    info_method!(image_max_buffer_size, CL_DEVICE_IMAGE_MAX_BUFFER_SIZE, usize);
    info_method!(image_pitch_alignment, CL_DEVICE_IMAGE_PITCH_ALIGNMENT, u32);
    info_method!(image_support, CL_DEVICE_IMAGE_SUPPORT, bool);
    info_method!(linker_available, CL_DEVICE_LINKER_AVAILABLE, bool);
    info_method!(local_mem_size, CL_DEVICE_LOCAL_MEM_SIZE, u64);
    info_method!(local_mem_type, CL_DEVICE_LOCAL_MEM_TYPE, DeviceLocalMemType);
    info_method!(max_clock_frequency, CL_DEVICE_MAX_CLOCK_FREQUENCY, u32);
    info_method!(max_compute_units, CL_DEVICE_MAX_COMPUTE_UNITS, u32);
    info_method!(max_constant_args, CL_DEVICE_MAX_CONSTANT_ARGS, u32);
    info_method!(max_constant_buffer_size, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, u64);
    info_method!(max_global_variable_size, CL_DEVICE_MAX_GLOBAL_VARIABLE_SIZE, usize);
    info_method!(max_mem_alloc_size, CL_DEVICE_MAX_MEM_ALLOC_SIZE, u64);
    info_method!(max_num_sub_groups, CL_DEVICE_MAX_NUM_SUB_GROUPS, u32);
    info_method!(max_on_device_events, CL_DEVICE_MAX_ON_DEVICE_EVENTS, u32);
    info_method!(max_on_device_queues, CL_DEVICE_MAX_ON_DEVICE_QUEUES, u32);
    info_method!(max_parameter_size, CL_DEVICE_MAX_PARAMETER_SIZE, usize);
    info_method!(max_pipe_args, CL_DEVICE_MAX_PIPE_ARGS, u32);
    info_method!(max_read_image_args, CL_DEVICE_MAX_READ_IMAGE_ARGS, u32);
    info_method!(max_read_write_image_args, CL_DEVICE_MAX_READ_WRITE_IMAGE_ARGS, u32);
    info_method!(max_samplers, CL_DEVICE_MAX_SAMPLERS, u32);
    info_method!(max_work_group_size, CL_DEVICE_MAX_WORK_GROUP_SIZE, usize);
    info_method!(max_work_item_dimensions, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, u32);
    info_method!(max_work_item_sizes, CL_DEVICE_MAX_WORK_ITEM_SIZES, Vec<usize>);
    info_method!(max_write_image_args, CL_DEVICE_MAX_WRITE_IMAGE_ARGS, u32);
    info_method!(mem_base_addr_align, CL_DEVICE_MEM_BASE_ADDR_ALIGN, u32);
    info_method!(name, CL_DEVICE_NAME, String);
    info_method!(native_vector_width_char, CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR, u32);
    info_method!(native_vector_width_short, CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT, u32);
    info_method!(native_vector_width_int, CL_DEVICE_NATIVE_VECTOR_WIDTH_INT, u32);
    info_method!(native_vector_width_long, CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG, u32);
    info_method!(native_vector_width_float, CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT, u32);
    info_method!(native_vector_width_double, CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE, u32);
    info_method!(native_vector_width_half, CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF, u32);
    info_method!(opencl_c_version, CL_DEVICE_OPENCL_C_VERSION, String);
    info_method!(parent_device, CL_DEVICE_PARENT_DEVICE, Device);
    info_method!(partition_affinity_domain, CL_DEVICE_PARTITION_AFFINITY_DOMAIN, DeviceAffinityDomain);
    info_method!(partition_max_sub_devices, CL_DEVICE_PARTITION_MAX_SUB_DEVICES, u32);
    // info_method!(partition_properties, CL_DEVICE_PARTITION_PROPERTIES, Vec<DevicePartitionProperty>);
    // info_method!(partition_type, CL_DEVICE_PARTITION_TYPE, Vec<DevicePartitionProperty>);
    info_method!(pipe_max_active_reservations, CL_DEVICE_PIPE_MAX_ACTIVE_RESERVATIONS, u32);
    info_method!(pipe_max_packet_size, CL_DEVICE_PIPE_MAX_PACKET_SIZE, u32);
    info_method!(platform, CL_DEVICE_PLATFORM, Platform);
    info_method!(preferred_global_atomic_alignment, CL_DEVICE_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT, u32);
    info_method!(preferred_interop_user_sync, CL_DEVICE_PREFERRED_INTEROP_USER_SYNC, bool);
    info_method!(preferred_local_atomic_alignment, CL_DEVICE_PREFERRED_LOCAL_ATOMIC_ALIGNMENT, u32);
    info_method!(preferred_platform_atomic_alignment, CL_DEVICE_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT, u32);
    info_method!(preferred_vector_width_char, CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, u32);
    info_method!(preferred_vector_width_short, CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, u32);
    info_method!(preferred_vector_width_int, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, u32);
    info_method!(preferred_vector_width_long, CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, u32);
    info_method!(preferred_vector_width_float, CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, u32);
    info_method!(preferred_vector_width_double, CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, u32);
    info_method!(preferred_vector_width_half, CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF, u32);
    info_method!(printf_buffer_size, CL_DEVICE_PRINTF_BUFFER_SIZE, usize);
    info_method!(profile, CL_DEVICE_PROFILE, String);
    info_method!(profiling_timer_resolution, CL_DEVICE_PROFILING_TIMER_RESOLUTION, usize);
    info_method!(queue_on_device_max_size, CL_DEVICE_QUEUE_ON_DEVICE_MAX_SIZE, u32);
    info_method!(queue_on_device_preferred_size, CL_DEVICE_QUEUE_ON_DEVICE_PREFERRED_SIZE, u32);
    info_method!(queue_on_device_properties, CL_DEVICE_QUEUE_ON_DEVICE_PROPERTIES, CommandQueueProperties);
    info_method!(queue_on_host_properties, CL_DEVICE_QUEUE_ON_HOST_PROPERTIES, CommandQueueProperties);
    info_method!(reference_count, CL_DEVICE_REFERENCE_COUNT, u32);
    info_method!(single_fp_config, CL_DEVICE_SINGLE_FP_CONFIG, DeviceFpConfig);
    info_method!(spir_versions, CL_DEVICE_SPIR_VERSIONS, String);
    info_method!(subgroup_independent_forward_progress, CL_DEVICE_SUB_GROUP_INDEPENDENT_FORWARD_PROGRESS, bool);
    info_method!(svm_capabilities, CL_DEVICE_SVM_CAPABILITIES, DeviceSvmCapabilities);
    // info_method!(terminate_capability_khr, CL_DEVICE_TERMINATE_CAPABILITY_KHR, DeviceTerminateCapabilityKhr); // TODO
    info_method!(get_type, CL_DEVICE_TYPE, DeviceType);
    info_method!(vendor, CL_DEVICE_VENDOR, String);
    info_method!(vendor_id, CL_DEVICE_VENDOR_ID, u32);
    info_method!(version, CL_DEVICE_VERSION, String);
    info_method!(driver_version, CL_DRIVER_VERSION, String);
}

trait Property {
    fn encode(&self) -> (isize, isize);
}

fn encode_propaties<P: Property>(properties: &[P]) -> Vec<isize> {
    let mut props = vec![0; 2 * (properties.len() + 1)];
    for i in 0..properties.len() {
        let (key, val) = properties[i].encode();
        props[i * 2 + 0] = key;
        props[i * 2 + 1] = val;
    }
    props
}

#[derive(Debug)]
pub struct Context(cl_context);

pub enum ContextProperty<'a> {
    Platform(&'a Platform),
    InteropUserSync(bool),
}

impl<'a> Property for ContextProperty<'a> {
    fn encode(&self) -> (isize, isize) {
        match self {
            &ContextProperty::Platform(ref p) => (CL_CONTEXT_PLATFORM as _, p.inner() as _),
            &ContextProperty::InteropUserSync(b) => (
                CL_CONTEXT_INTEROP_USER_SYNC as _,
                if b { 1 } else { 0 } as _,
            ),
        }
    }
}

impl Object for Context {
    type InnerType = cl_context;
    fn inner(&self) -> Self::InnerType {
        self.0
    }
    const GET_FN: InfoFn<Self::InnerType> = clGetContextInfo;
}

impl Drop for Context {
    fn drop(&mut self) {
        to_clerror(unsafe { clReleaseContext(self.0) }).unwrap();
    }
}

impl InfoType for Context {
    type RawType = cl_context;
    // FIXME: should be increment reference_count?
    fn from_raw(val: Self::RawType) -> Self {
        Context(val)
    }
}

impl Context {
    // TODO: callback function
    pub fn with_devices(devices: &[&Device], properties: &[ContextProperty]) -> Result<Context> {
        let props = encode_propaties(properties);
        let devs = to_inner_vec(devices);
        let mut err = 0;
        let ctx = unsafe {
            clCreateContext(
                props.as_ptr(),
                devices.len() as u32,
                devs.as_ptr(),
                None,
                null_mut(),
                &mut err,
            )
        };
        to_clerror(err)?;
        Ok(Context(ctx))
    }

    pub fn with_type(ty: DeviceType, properties: &[ContextProperty]) -> Result<Context> {
        let props = encode_propaties(properties);
        let mut err = 0;
        let ctx = unsafe {
            clCreateContextFromType(
                props.as_ptr(),
                ty as cl_device_type,
                None,
                null_mut(),
                &mut err,
            )
        };
        to_clerror(err)?;
        Ok(Context(ctx))
    }

    // default
    // set_default

    info_method!(reference_count, CL_CONTEXT_REFERENCE_COUNT, u32);
    info_method!(num_devices, CL_CONTEXT_NUM_DEVICES, u32);
    info_method!(devices, CL_CONTEXT_DEVICES, Vec<Device>);
    // info_method!(properties, CL_CONTEXT_PROPERTIES, Vec<ContextProperty>);
}

#[derive(Debug)]
pub struct CommandQueue(cl_command_queue);

pub enum QueueProperty {
    Properties(CommandQueueProperties),
    Size(u32),
    // PriorityKHR(cl_queue_priority_khr),
    // ThrottleKHR,
}

impl Property for QueueProperty {
    fn encode(&self) -> (isize, isize) {
        match self {
            &QueueProperty::Properties(p) => (CL_QUEUE_PROPERTIES as _, p.bits() as _),
            &QueueProperty::Size(n) => (CL_QUEUE_SIZE as _, n as _),
        }
    }
}

impl Object for CommandQueue {
    type InnerType = cl_command_queue;
    fn inner(&self) -> Self::InnerType {
        self.0
    }
    const GET_FN: InfoFn<Self::InnerType> = clGetCommandQueueInfo;
}

impl Drop for CommandQueue {
    fn drop(&mut self) {
        to_clerror(unsafe { clReleaseCommandQueue(self.0) }).unwrap();
    }
}

impl InfoType for CommandQueue {
    type RawType = cl_command_queue;
    fn from_raw(val: Self::RawType) -> Self {
        CommandQueue(val)
    }
}

impl CommandQueue {
    pub fn with_context_and_device(
        context: &Context,
        device: &Device,
        properties: &[QueueProperty],
    ) -> Result<CommandQueue> {
        let mut err = 0;
        let q = unsafe {
            clCreateCommandQueueWithProperties(
                context.inner(),
                device.inner(),
                encode_propaties(properties).as_ptr() as _,
                &mut err,
            )
        };
        to_clerror(err)?;
        Ok(CommandQueue(q))
    }

    pub fn with_context(context: &Context, properties: &[QueueProperty]) -> Result<CommandQueue> {
        Self::with_context_and_device(
            context,
            context
                .devices()?
                .first()
                .ok_or(ErrorKind::InvalidParameter(
                    "Context has no device".to_owned(),
                ))?,
            properties,
        )
    }

    // default
    // set_default

    pub fn flush(&self) -> Result<()> {
        check!(clFlush(self.inner()));
        Ok(())
    }

    pub fn finish(&self) -> Result<()> {
        check!(clFinish(self.inner()));
        Ok(())
    }

    pub fn enqueue_read_buffer<T>(
        &self,
        buffer: &Buffer,
        blocking_read: bool,
        offset: usize,
        size: usize,
        ptr: &mut [T],
        event_wait_list: &[&Event],
    ) -> Result<Event> {
        let ewl = to_inner_vec(event_wait_list);
        let mut ev = null_mut();
        check!(clEnqueueReadBuffer(
            self.inner(),
            buffer.inner(),
            blocking_read as cl_bool,
            offset,
            size,
            ptr.as_mut_ptr() as _,
            ewl.len() as u32,
            if ewl.is_empty() {
                null()
            } else {
                ewl.as_ptr()
            },
            &mut ev
        ));
        Ok(Event(ev))
    }

    pub fn enqueue_read_buffer_rect<T>(
        &self,
        buffer: &Buffer,
        blocking_read: bool,
        buffer_offset: &[usize; 3],
        host_offset: &[usize; 3],
        region: &[usize; 3],
        buffer_row_pitch: usize,
        buffer_slice_pitch: usize,
        host_row_pitch: usize,
        host_slice_pitch: usize,
        ptr: &mut [T],
        event_wait_list: &[&Event],
    ) -> Result<Event> {
        let ewl = to_inner_vec(event_wait_list);
        let mut ev = null_mut();
        check!(clEnqueueReadBufferRect(
            self.inner(),
            buffer.inner(),
            blocking_read as cl_bool,
            buffer_offset.as_ptr(),
            host_offset.as_ptr(),
            region.as_ptr(),
            buffer_row_pitch,
            buffer_slice_pitch,
            host_row_pitch,
            host_slice_pitch,
            ptr.as_mut_ptr() as _,
            ewl.len() as u32,
            if ewl.is_empty() {
                null()
            } else {
                ewl.as_ptr()
            },
            &mut ev
        ));
        Ok(Event(ev))
    }

    pub fn enqueue_write_buffer<T>(
        &self,
        buffer: &Buffer,
        blocking_write: bool,
        offset: usize,
        size: usize,
        ptr: &[T],
        event_wait_list: &[&Event],
    ) -> Result<Event> {
        let ewl = to_inner_vec(event_wait_list);
        let mut ev = null_mut();
        check!(clEnqueueWriteBuffer(
            self.inner(),
            buffer.inner(),
            blocking_write as cl_bool,
            offset,
            size,
            ptr.as_ptr() as _,
            ewl.len() as u32,
            if ewl.is_empty() {
                null()
            } else {
                ewl.as_ptr()
            },
            &mut ev
        ));
        Ok(Event(ev))
    }

    pub fn enqueue_write_buffer_rect<T>(
        &self,
        buffer: &Buffer,
        blocking_write: bool,
        buffer_offset: &[usize; 3],
        host_offset: &[usize; 3],
        region: &[usize; 3],
        buffer_row_pitch: usize,
        buffer_slice_pitch: usize,
        host_row_pitch: usize,
        host_slice_pitch: usize,
        ptr: &[T],
        event_wait_list: &[&Event],
    ) -> Result<Event> {
        let ewl = to_inner_vec(event_wait_list);
        let mut ev = null_mut();
        check!(clEnqueueWriteBufferRect(
            self.inner(),
            buffer.inner(),
            blocking_write as cl_bool,
            buffer_offset.as_ptr(),
            host_offset.as_ptr(),
            region.as_ptr(),
            buffer_row_pitch,
            buffer_slice_pitch,
            host_row_pitch,
            host_slice_pitch,
            ptr.as_ptr() as _,
            ewl.len() as u32,
            if ewl.is_empty() {
                null()
            } else {
                ewl.as_ptr()
            },
            &mut ev
        ));
        Ok(Event(ev))
    }

    pub fn enqueue_copy_buffer<T>(
        &self,
        src_buffer: &Buffer,
        dst_buffer: &Buffer,
        src_offset: usize,
        dst_offset: usize,
        size: usize,
        event_wait_list: &[&Event],
    ) -> Result<Event> {
        let ewl = to_inner_vec(event_wait_list);
        let mut ev = null_mut();
        check!(clEnqueueCopyBuffer(
            self.inner(),
            src_buffer.inner(),
            dst_buffer.inner(),
            src_offset,
            dst_offset,
            size,
            ewl.len() as u32,
            if ewl.is_empty() {
                null()
            } else {
                ewl.as_ptr()
            },
            &mut ev
        ));
        Ok(Event(ev))
    }

    pub fn enqueue_copy_buffer_rect<T>(
        &self,
        src_buffer: &Buffer,
        dst_buffer: &Buffer,
        src_origin: &[usize; 3],
        dst_origin: &[usize; 3],
        region: &[usize; 3],
        src_row_pitch: usize,
        src_slice_pitch: usize,
        dst_row_pitch: usize,
        dst_slice_pitch: usize,
        event_wait_list: &[&Event],
    ) -> Result<Event> {
        let ewl = to_inner_vec(event_wait_list);
        let mut ev = null_mut();
        check!(clEnqueueCopyBufferRect(
            self.inner(),
            src_buffer.inner(),
            dst_buffer.inner(),
            src_origin.as_ptr(),
            dst_origin.as_ptr(),
            region.as_ptr(),
            src_row_pitch,
            src_slice_pitch,
            dst_row_pitch,
            dst_slice_pitch,
            ewl.len() as u32,
            if ewl.is_empty() {
                null()
            } else {
                ewl.as_ptr()
            },
            &mut ev
        ));
        Ok(Event(ev))
    }

    // TODO: clEnqueueFillBuffer

    // TODO: read/write/copy images

    // TODO: map buffer/image

    pub fn enqueue_ndrange_kernel(
        &self,
        kernel: &Kernel,
        global_work_offset: &[usize],
        global_work_size: &[usize],
        local_work_size: &[usize],
        event_wait_list: &[&Event],
    ) -> Result<Event> {
        assert!(global_work_size.len() > 0);
        assert!(
            global_work_offset.len() == global_work_size.len() || global_work_offset.len() == 0
        );
        assert!(local_work_size.len() == global_work_size.len() || local_work_size.len() == 0);

        let work_dim = global_work_size.len() as cl_uint;
        let ewl: Vec<cl_event> = event_wait_list.iter().map(|e| e.inner()).collect();
        let mut ev = null_mut();
        check!(clEnqueueNDRangeKernel(
            self.inner(),
            kernel.inner(),
            work_dim,
            if global_work_offset.is_empty() {
                null()
            } else {
                global_work_offset.as_ptr()
            },
            global_work_size.as_ptr(),
            if local_work_size.is_empty() {
                null()
            } else {
                local_work_size.as_ptr()
            },
            ewl.len() as u32,
            if ewl.is_empty() {
                null()
            } else {
                ewl.as_ptr()
            },
            &mut ev
        ));
        Ok(Event(ev))
    }

    // enqueue_native_kernel

    info_method!(context, CL_QUEUE_CONTEXT, Context);
    info_method!(device, CL_QUEUE_DEVICE, Device);
    info_method!(reference_count, CL_QUEUE_REFERENCE_COUNT, u32);
    info_method!(properties, CL_QUEUE_PROPERTIES, CommandQueueProperties);
    info_method!(size, CL_QUEUE_SIZE, u32);
    info_method!(device_default, CL_QUEUE_DEVICE_DEFAULT, CommandQueue);
}

#[derive(Debug)]
pub struct Buffer(cl_mem);

bitflags! {
    pub struct MemFlags: cl_bitfield {
        const READ_WRITE = CL_MEM_READ_WRITE as cl_bitfield;
        const WRITE_ONLY = CL_MEM_WRITE_ONLY as cl_bitfield;
        const READ_ONLY = CL_MEM_READ_ONLY as cl_bitfield;
        const USE_HOST_PTR = CL_MEM_USE_HOST_PTR as cl_bitfield;
        const ALLOC_HOST_PTR = CL_MEM_ALLOC_HOST_PTR as cl_bitfield;
        const COPY_HOST_PTR = CL_MEM_COPY_HOST_PTR as cl_bitfield;
        const HOST_WRITE_ONLY = CL_MEM_HOST_WRITE_ONLY as cl_bitfield;
        const HOST_READ_ONLY = CL_MEM_HOST_READ_ONLY as cl_bitfield;
        const HOST_NO_ACCESS = CL_MEM_HOST_NO_ACCESS as cl_bitfield;
        const SVM_FINE_GRAIN_BUFFER = CL_MEM_SVM_FINE_GRAIN_BUFFER as cl_bitfield;
        const SVM_ATOMICS = CL_MEM_SVM_ATOMICS as cl_bitfield;
        const KERNEL_READ_AND_WRITE = CL_MEM_KERNEL_READ_AND_WRITE as cl_bitfield;
    }
}

impl_info_type_bitfield!(MemFlags);

#[derive(Copy, Clone, Debug, FromPrimitive)]
pub enum MemType {
    Buffer = CL_MEM_OBJECT_BUFFER as isize,
    Image2D = CL_MEM_OBJECT_IMAGE2D as isize,
    Image3D = CL_MEM_OBJECT_IMAGE3D as isize,
    Image2DArray = CL_MEM_OBJECT_IMAGE2D_ARRAY as isize,
    Image1D = CL_MEM_OBJECT_IMAGE1D as isize,
    Image1DArray = CL_MEM_OBJECT_IMAGE1D_ARRAY as isize,
    Iage1DBuffer = CL_MEM_OBJECT_IMAGE1D_BUFFER as isize,
    Pipe = CL_MEM_OBJECT_PIPE as isize,
}

impl_info_type_enum!(MemType, cl_mem_object_type);

impl Object for Buffer {
    type InnerType = cl_mem;
    fn inner(&self) -> Self::InnerType {
        self.0
    }
    const GET_FN: InfoFn<Self::InnerType> = clGetMemObjectInfo;
}

impl Drop for Buffer {
    fn drop(&mut self) {
        to_clerror(unsafe { clReleaseMemObject(self.0) }).unwrap();
    }
}

impl InfoType for Buffer {
    type RawType = cl_mem;
    fn from_raw(val: Self::RawType) -> Self {
        Buffer(val)
    }
}

impl Buffer {
    pub fn with_size(context: &Context, flags: MemFlags, size: usize) -> Result<Buffer> {
        let mut err = 0;
        let buf = unsafe {
            clCreateBuffer(
                context.inner(),
                flags.bits() as cl_mem_flags,
                size,
                null_mut(),
                &mut err,
            )
        };
        to_clerror(err)?;
        Ok(Buffer(buf))
    }

    pub fn with_host_ptr<T>(
        context: &Context,
        flags: MemFlags,
        host_ptr: &mut [T],
    ) -> Result<Buffer> {
        let mut err = 0;
        let buf = unsafe {
            clCreateBuffer(
                context.inner(),
                flags.bits() as cl_mem_flags,
                host_ptr.len(),
                host_ptr.as_mut_ptr() as _,
                &mut err,
            )
        };
        to_clerror(err)?;
        Ok(Buffer(buf))
    }

    // create_sub_buffer

    info_method!(get_type, CL_MEM_TYPE, MemType);
    info_method!(flags, CL_MEM_FLAGS, MemFlags);
    info_method!(size, CL_MEM_SIZE, usize);
    info_method!(host_ptr, CL_MEM_HOST_PTR, *mut ());
    info_method!(map_count, CL_MEM_MAP_COUNT, u32);
    info_method!(reference_count, CL_MEM_REFERENCE_COUNT, u32);
    info_method!(context, CL_MEM_CONTEXT, Context);
    info_method!(
        associated_memobject,
        CL_MEM_ASSOCIATED_MEMOBJECT,
        Option<Buffer>
    );
    info_method!(offset, CL_MEM_OFFSET, usize);
    info_method!(uses_svm_pointer, CL_MEM_USES_SVM_POINTER, bool);
}

// Sampler

#[derive(Debug)]
pub struct Program(cl_program);

impl Object for Program {
    type InnerType = cl_program;
    fn inner(&self) -> Self::InnerType {
        self.0
    }
    const GET_FN: InfoFn<Self::InnerType> = clGetProgramInfo;
}

impl Drop for Program {
    fn drop(&mut self) {
        to_clerror(unsafe { clReleaseProgram(self.0) }).unwrap();
    }
}

impl InfoType for Program {
    type RawType = cl_program;
    fn from_raw(val: Self::RawType) -> Self {
        Program(val)
    }
}

impl Program {
    pub fn with_source<S: AsRef<str>>(context: &Context, sources: &[S]) -> Result<Program> {
        let mut err = 0;
        let cstrs = sources
            .iter()
            .map(|src| str_to_cstring(src.as_ref()))
            .collect::<Result<Vec<CString>>>()?;
        let mut ptrs: Vec<_> = cstrs.into_iter().map(|cs| cs.as_ptr()).collect();
        let lens: Vec<_> = sources.iter().map(|src| src.as_ref().len()).collect();
        let program = unsafe {
            clCreateProgramWithSource(
                context.inner(),
                sources.len() as cl_uint,
                ptrs.as_mut_ptr(),
                lens.as_ptr(),
                &mut err,
            )
        };
        to_clerror(err)?;
        Ok(Program(program))
    }

    pub fn with_binary<S: AsRef<[u8]>>(
        context: &Context,
        device_and_binaries: &[(&Device, S)],
    ) -> Result<Program> {
        let mut bin_stats = vec![0; device_and_binaries.len()];
        let mut err = 0;
        let devs: Vec<_> = device_and_binaries.iter().map(|p| p.0.inner()).collect();
        let lens: Vec<_> = device_and_binaries
            .iter()
            .map(|p| p.1.as_ref().len())
            .collect();
        let mut bins: Vec<_> = device_and_binaries
            .iter()
            .map(|p| p.1.as_ref().as_ptr())
            .collect();
        let program = unsafe {
            clCreateProgramWithBinary(
                context.inner(),
                device_and_binaries.len() as cl_uint,
                devs.as_ptr(),
                lens.as_ptr(),
                bins.as_mut_ptr(),
                bin_stats.as_mut_ptr(),
                &mut err,
            )
        };
        to_clerror(err)?;
        for bin_stat in bin_stats {
            to_clerror(bin_stat)?;
        }
        Ok(Program(program))
    }

    // with_builtin_kernel
    // with_il

    // TODO: callback
    pub fn build(&self, devices: &[&Device], options: &str) -> Result<()> {
        let devs = to_inner_vec(devices);
        check!(clBuildProgram(
            self.inner(),
            devs.len() as cl_uint,
            devs.as_ptr(),
            str_to_cstring(options)?.as_ptr(),
            None,
            null_mut()
        ));
        Ok(())
    }

    pub fn compile<S: AsRef<str>>(
        &self,
        devices: &[&Device],
        options: &str,
        input_headers: &[&Program],
        header_include_names: &[S],
    ) -> Result<()> {
        assert!(
            header_include_names.len() == 0 || header_include_names.len() == input_headers.len()
        );
        let devs = to_inner_vec(devices);
        let progs = to_inner_vec(input_headers);
        let mut headers = header_include_names
            .iter()
            .map(|h| str_to_cstring(h.as_ref()))
            .collect::<Result<Vec<CString>>>()?;
        check!(clCompileProgram(
            self.inner(),
            devs.len() as cl_uint,
            devs.as_ptr(),
            str_to_cstring(options)?.as_ptr(),
            progs.len() as cl_uint,
            progs.as_ptr(),
            if headers.len() == 0 {
                null_mut()
            } else {
                headers.as_mut_ptr() as _
            },
            None,
            null_mut()
        ));
        Ok(())
    }

    pub fn link(
        context: &Context,
        devices: &[&Device],
        options: &str,
        input_programs: &[&Program],
    ) -> Result<Program> {
        let devs = to_inner_vec(devices);
        let progs = to_inner_vec(input_programs);
        let mut err = 0;
        let program = unsafe {
            clLinkProgram(
                context.inner(),
                devs.len() as cl_uint,
                devs.as_ptr(),
                str_to_cstring(options)?.as_ptr(),
                progs.len() as cl_uint,
                progs.as_ptr(),
                None,
                null_mut(),
                &mut err,
            )
        };
        to_clerror(err)?;
        Ok(Program(program))
    }

    pub fn kernels(&self) -> Result<Vec<Kernel>> {
        let ks = get_objects(|a, b, c| unsafe { clCreateKernelsInProgram(self.inner(), a, b, c) })?;
        Ok(ks.iter().map(|&k| Kernel(k)).collect())
    }

    // fn set_release_callback

    // fn set_specialization_constant

    // pub fn build_info()

    info_method!(reference_count, CL_PROGRAM_REFERENCE_COUNT, u32);
    info_method!(context, CL_PROGRAM_CONTEXT, Context);
    info_method!(num_devices, CL_PROGRAM_NUM_DEVICES, u32);
    info_method!(devices, CL_PROGRAM_DEVICES, Vec<Device>);
    info_method!(source, CL_PROGRAM_SOURCE, String);
    info_method!(binary_sizes, CL_PROGRAM_BINARY_SIZES, Vec<usize>);
    // FIXME: should return Vec<Vec<u8>>?
    // info_method!(binaries, CL_PROGRAM_BINARIES, Vec<*const u8>);
    info_method!(num_kernels, CL_PROGRAM_NUM_KERNELS, usize);
    info_method!(kernel_names, CL_PROGRAM_KERNEL_NAMES, String);
    // TODO
    // info_method!(il, CL_PROGRAM_IL, );
    // info_method!(scope_global_ctors_present, CL_PROGRAM_SCOPE_GLOBAL_CTORS_PRESENT, );
    // info_method!(scope_global_dtors_present, CL_PROGRAM_SCOPE_GLOBAL_DTORS_PRESENT, );
}

#[derive(Debug)]
pub struct Kernel(cl_kernel);

impl Object for Kernel {
    type InnerType = cl_kernel;
    fn inner(&self) -> Self::InnerType {
        self.0
    }
    const GET_FN: InfoFn<Self::InnerType> = clGetKernelInfo;
}

impl Drop for Kernel {
    fn drop(&mut self) {
        to_clerror(unsafe { clReleaseKernel(self.0) }).unwrap();
    }
}

pub trait KernelArg: Sized {
    fn size(&self) -> usize {
        std::mem::size_of::<Self>()
    }
    fn ptr(&self) -> *const () {
        self as *const Self as _
    }
}

impl KernelArg for i32 {}
impl KernelArg for u32 {}
impl KernelArg for i64 {}
impl KernelArg for u64 {}
impl KernelArg for isize {}
impl KernelArg for usize {}
impl KernelArg for f32 {}
impl KernelArg for f64 {}

impl KernelArg for Buffer {
    fn size(&self) -> usize {
        std::mem::size_of::<cl_mem>()
    }
    fn ptr(&self) -> *const () {
        &self.0 as *const _ as _
    }
}

#[derive(Copy, Clone, Debug, FromPrimitive)]
pub enum ArgAddressQualifier {
    Global = CL_KERNEL_ARG_ADDRESS_GLOBAL as isize,
    Local = CL_KERNEL_ARG_ADDRESS_LOCAL as isize,
    Constant = CL_KERNEL_ARG_ADDRESS_CONSTANT as isize,
    Private = CL_KERNEL_ARG_ADDRESS_PRIVATE as isize,
}
impl_info_type_enum!(ArgAddressQualifier, cl_kernel_arg_address_qualifier);

#[derive(Copy, Clone, Debug, FromPrimitive)]
pub enum ArgAccessQualifier {
    ReadOnly = CL_KERNEL_ARG_ACCESS_READ_ONLY as isize,
    WriteOnly = CL_KERNEL_ARG_ACCESS_WRITE_ONLY as isize,
    ReadWrite = CL_KERNEL_ARG_ACCESS_READ_WRITE as isize,
    None = CL_KERNEL_ARG_ACCESS_NONE as isize,
}
impl_info_type_enum!(ArgAccessQualifier, cl_kernel_arg_access_qualifier);

bitflags! {
    pub struct ArgTypeQualifier: cl_bitfield {
        const NONE = CL_KERNEL_ARG_TYPE_NONE as cl_bitfield;
        const CONST = CL_KERNEL_ARG_TYPE_CONST as cl_bitfield;
        const RESTRICT = CL_KERNEL_ARG_TYPE_RESTRICT as cl_bitfield;
        const VOLATILE = CL_KERNEL_ARG_TYPE_VOLATILE as cl_bitfield;
        const PIPE = CL_KERNEL_ARG_TYPE_PIPE as cl_bitfield;
    }
}
impl_info_type_bitfield!(ArgTypeQualifier);

macro_rules! arg_info {
    ($method:ident, $name:expr, $ret:ty) => {
        pub fn $method(&self, index: u32) -> Result<$ret> {
            InfoType::get_info(|a, b, c| unsafe {
                clGetKernelArgInfo(self.inner(), index, $name, a, b, c)
            })
        }
    }
}

impl Kernel {
    // TODO: clCloneKernel

    pub fn new(program: &Program, kernel_name: &str) -> Result<Kernel> {
        let mut err = 0;
        let kernel = unsafe {
            clCreateKernel(
                program.inner(),
                str_to_cstring(kernel_name)?.as_ptr(),
                &mut err,
            )
        };
        to_clerror(err)?;
        Ok(Kernel(kernel))
    }

    pub fn set_arg<T: KernelArg>(&self, index: u32, value: &T) -> Result<()> {
        check!(clSetKernelArg(
            self.inner(),
            index,
            value.size(),
            value.ptr() as _
        ));
        Ok(())
    }

    info_method!(function_name, CL_KERNEL_FUNCTION_NAME, String);
    info_method!(num_args, CL_KERNEL_NUM_ARGS, u32);
    info_method!(reference_count, CL_KERNEL_REFERENCE_COUNT, u32);
    info_method!(context, CL_KERNEL_CONTEXT, Context);
    info_method!(program, CL_KERNEL_PROGRAM, Program);
    info_method!(attributes, CL_KERNEL_ATTRIBUTES, String);

    arg_info!(
        arg_address_qualifier,
        CL_KERNEL_ARG_ADDRESS_QUALIFIER,
        ArgAddressQualifier
    );
    arg_info!(
        arg_access_qualifier,
        CL_KERNEL_ARG_ACCESS_QUALIFIER,
        ArgAccessQualifier
    );
    arg_info!(arg_type_name, CL_KERNEL_ARG_TYPE_NAME, String);
    arg_info!(
        arg_type_qualifier,
        CL_KERNEL_ARG_TYPE_QUALIFIER,
        ArgTypeQualifier
    );
    arg_info!(arg_name, CL_KERNEL_ARG_NAME, String);

    // clGetKernelWorkGroupInfo
    // clGetKernelSubGroupInfo
}

#[macro_export]
macro_rules! make_kernel_impl {
    ($program:expr, $name:expr;
        $arg_type:ty;
        $arg_value:ident, $($arg_values:ident),*;
        $(($types:ty, $values:ident))*
    ) => {
        {
            let kernel = $crate::Kernel::new($program, $name)?;
            let closure = move |
                queue: &$crate::CommandQueue,
                global_work_size: &[usize],
                $($values: $types),*,
                $arg_value: $arg_type,
            | -> $crate::Result<$crate::Event> {
                let mut i = 0;
                $(
                    kernel.set_arg(i, $values)?;
                    i += 1;
                )*
                kernel.set_arg(i, $arg_value)?;
                queue.enqueue_ndrange_kernel(&kernel, &[], global_work_size, &[], &[])
            };
            Box::new(closure)
        }
    };

    ($program:expr, $name:expr;
        $arg_type:ty, $($arg_types:ty),*;
        $arg_value:ident, $($arg_values:ident),*;
        $($params:tt)*
    ) => {
        make_kernel_impl!(
            $program, $name;
            $($arg_types),*;
            $($arg_values),*;
            $($params)* ($arg_type, $arg_value)
        )
    };
}

#[macro_export]
macro_rules! make_kernel {
    ($program:expr, $name:expr, $($arg_types:ty),*) => {
        make_kernel_impl!(
            $program, $name;
            $($arg_types),*;
            arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8,
            arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16,
            arg17, arg18, arg19, arg20, arg21, arg22, arg23, arg24,
            arg25, arg26, arg27, arg28, arg29, arg30, arg31, arg32;
        )
    };
}

#[derive(Debug)]
pub struct Event(cl_event);

impl Object for Event {
    type InnerType = cl_event;
    fn inner(&self) -> Self::InnerType {
        self.0
    }
    const GET_FN: InfoFn<Self::InnerType> = clGetEventInfo;
}

impl Drop for Event {
    fn drop(&mut self) {
        to_clerror(unsafe { clReleaseEvent(self.0) }).unwrap();
    }
}

#[derive(Copy, Clone, Debug, FromPrimitive)]
pub enum CommandType {
    NDRangeKernel = CL_COMMAND_NDRANGE_KERNEL as isize,
    Task = CL_COMMAND_TASK as isize,
    NativeKernel = CL_COMMAND_NATIVE_KERNEL as isize,
    ReadBuffer = CL_COMMAND_READ_BUFFER as isize,
    WriteBuffer = CL_COMMAND_WRITE_BUFFER as isize,
    CopyBuffer = CL_COMMAND_COPY_BUFFER as isize,
    ReadImage = CL_COMMAND_READ_IMAGE as isize,
    WriteImage = CL_COMMAND_WRITE_IMAGE as isize,
    CopyImage = CL_COMMAND_COPY_IMAGE as isize,
    CopyImageToBuffer = CL_COMMAND_COPY_IMAGE_TO_BUFFER as isize,
    CopyBufferToImage = CL_COMMAND_COPY_BUFFER_TO_IMAGE as isize,
    MapBuffer = CL_COMMAND_MAP_BUFFER as isize,
    MapImage = CL_COMMAND_MAP_IMAGE as isize,
    UnmapMemObject = CL_COMMAND_UNMAP_MEM_OBJECT as isize,
    Marker = CL_COMMAND_MARKER as isize,
    AcquireGlObjects = CL_COMMAND_ACQUIRE_GL_OBJECTS as isize,
    ReleaseGlObjects = CL_COMMAND_RELEASE_GL_OBJECTS as isize,
    ReadBufferRect = CL_COMMAND_READ_BUFFER_RECT as isize,
    WriteBufferRect = CL_COMMAND_WRITE_BUFFER_RECT as isize,
    CopyBufferRect = CL_COMMAND_COPY_BUFFER_RECT as isize,
    User = CL_COMMAND_USER as isize,
    Barrier = CL_COMMAND_BARRIER as isize,
    MigrateMemObjects = CL_COMMAND_MIGRATE_MEM_OBJECTS as isize,
    FillBuffer = CL_COMMAND_FILL_BUFFER as isize,
    FillImage = CL_COMMAND_FILL_IMAGE as isize,
    SvmFree = CL_COMMAND_SVM_FREE as isize,
    SvmMemcpy = CL_COMMAND_SVM_MEMCPY as isize,
    SvmMemfill = CL_COMMAND_SVM_MEMFILL as isize,
    SvmMap = CL_COMMAND_SVM_MAP as isize,
    SvmUnmap = CL_COMMAND_SVM_UNMAP as isize,
}

impl_info_type_enum!(CommandType, cl_command_type);

#[derive(Copy, Clone, Debug, FromPrimitive)]
pub enum CommandExecutionStatus {
    Complete = CL_COMPLETE as isize,
    Running = CL_RUNNING as isize,
    Submitted = CL_SUBMITTED as isize,
    Queued = CL_QUEUED as isize,
}

impl_info_type_enum!(CommandExecutionStatus, cl_int);

impl Event {
    // FIXME: split as UserEvent?
    pub fn create_user_event(context: &Context) -> Result<Event> {
        let mut err = 0;
        let ev = unsafe { clCreateUserEvent(context.inner(), &mut err) };
        to_clerror(err)?;
        Ok(Event(ev))
    }

    // FIXME: split as UserEvent?
    pub fn set_status(&self, execution_status: CommandExecutionStatus) -> Result<()> {
        check!(clSetUserEventStatus(
            self.inner(),
            execution_status as cl_int
        ));
        Ok(())
    }

    pub fn wait(&self) -> Result<()> {
        wait_for_events(&[self])
    }

    // pub fn profiling_info(&self) -> Result<ProfilingInfo> {
    //     unimplemented!()
    // }

    // fn set_callback() -> Result<()> { }

    info_method!(command_queue, CL_EVENT_COMMAND_QUEUE, Option<CommandQueue>);
    info_method!(command_type, CL_EVENT_COMMAND_TYPE, CommandType);
    info_method!(reference_count, CL_EVENT_REFERENCE_COUNT, u32);
    info_method!(
        command_execution_status,
        CL_EVENT_COMMAND_EXECUTION_STATUS,
        CommandExecutionStatus
    );
    info_method!(context, CL_EVENT_CONTEXT, Context);
}

pub fn wait_for_events(events: &[&Event]) -> Result<()> {
    let es: Vec<_> = events.iter().map(|e| e.inner()).collect();
    check!(clWaitForEvents(es.len() as u32, es.as_ptr()));
    Ok(())
}

#[derive(Debug)]
pub struct Sampler(cl_sampler);

impl Object for Sampler {
    type InnerType = cl_sampler;
    fn inner(&self) -> Self::InnerType {
        self.0
    }
    const GET_FN: InfoFn<Self::InnerType> = clGetSamplerInfo;
}

impl Drop for Sampler {
    fn drop(&mut self) {
        to_clerror(unsafe { clReleaseSampler(self.0) }).unwrap();
    }
}

pub enum SamplerProperty {
    NormalizedCoords(bool),
    AddressingMode(AddressingMode),
    FilterMode(FilterMode),
}

impl Property for SamplerProperty {
    fn encode(&self) -> (isize, isize) {
        match self {
            &SamplerProperty::NormalizedCoords(a) => (CL_SAMPLER_NORMALIZED_COORDS as _, a as _),
            &SamplerProperty::AddressingMode(a) => (CL_SAMPLER_ADDRESSING_MODE as _, a as _),
            &SamplerProperty::FilterMode(a) => (CL_SAMPLER_FILTER_MODE as _, a as _),
        }
    }
}

#[derive(Copy, Clone, Debug, FromPrimitive)]
pub enum AddressingMode {
    None = CL_ADDRESS_NONE as isize,
    ClampToEdge = CL_ADDRESS_CLAMP_TO_EDGE as isize,
    Clamp = CL_ADDRESS_CLAMP as isize,
    Repeat = CL_ADDRESS_REPEAT as isize,
    MirroredRepeat = CL_ADDRESS_MIRRORED_REPEAT as isize,
}
impl_info_type_enum!(AddressingMode, cl_addressing_mode);

#[derive(Copy, Clone, Debug, FromPrimitive)]
pub enum FilterMode {
    Nearest = CL_FILTER_NEAREST as isize,
    Linear = CL_FILTER_LINEAR as isize,
}
impl_info_type_enum!(FilterMode, cl_filter_mode);

impl Sampler {
    pub fn new(context: &Context, properties: &[SamplerProperty]) -> Result<Sampler> {
        let mut err = 0;
        let sampler = unsafe {
            clCreateSamplerWithProperties(
                context.inner(),
                encode_propaties(properties).as_ptr() as _,
                &mut err,
            )
        };
        to_clerror(err)?;
        Ok(Sampler(sampler))
    }

    info_method!(reference_count, CL_SAMPLER_REFERENCE_COUNT, u32);
    info_method!(context, CL_SAMPLER_CONTEXT, Context);
    info_method!(normalized_coords, CL_SAMPLER_NORMALIZED_COORDS, bool);
    info_method!(addressing_mode, CL_SAMPLER_ADDRESSING_MODE, AddressingMode);
    info_method!(filter_mode, CL_SAMPLER_FILTER_MODE, FilterMode);
    // cl_khr_mipmap_image
    // info_method!(mip_filter_mode, CL_SAMPLER_MIP_FILTER_MODE, FilterMode);
    // info_method!(lod_min, CL_SAMPLER_LOD_MIN, f32);
    // info_method!(lod_max, CL_SAMPLER_LOD_MAX, f32);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        // test().unwrap();
    }

    #[cfg_attr(rustfmt, rustfmt_skip)]
    fn test() -> Result<()> {
        let platform = Platform::first()?.unwrap();
        println!("Platform: {:?}", platform);
        println!("  CL_PLATFORM_PROFILE   : {}", platform.profile()?);
        println!("  CL_PLATFORM_VERSION   : {}", platform.version()?);
        println!("  CL_PLATFORM_NAME      : {}", platform.name()?);
        println!("  CL_PLATFORM_VENDOR    : {}", platform.vendor()?);
        println!("  CL_PLATFORM_EXTENSIONS: {}", platform.extensions()?);

        let devices = platform.devices(DeviceType::All)?;
        println!("  Devices: {:?}", devices);
        assert!(devices.len() > 0);
        println!();

        let device = &devices[0];
        println!("Device: {:?}", device);
        println!("  CL_DEVICE_TYPE                                  : {:?}", device.get_type());
        println!("  CL_DEVICE_VENDOR_ID                             : {:?}", device.vendor_id());
        println!("  CL_DEVICE_MAX_COMPUTE_UNITS                     : {:?}", device.max_compute_units());
        println!("  CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS              : {:?}", device.max_work_item_dimensions());
        println!("  CL_DEVICE_MAX_WORK_GROUP_SIZE                   : {:?}", device.max_work_group_size());
        println!("  CL_DEVICE_MAX_WORK_ITEM_SIZES                   : {:?}", device.max_work_item_sizes());
        println!("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR           : {:?}", device.preferred_vector_width_char());
        println!("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT          : {:?}", device.preferred_vector_width_short());
        println!("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT            : {:?}", device.preferred_vector_width_int());
        println!("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG           : {:?}", device.preferred_vector_width_long());
        println!("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT          : {:?}", device.preferred_vector_width_float());
        println!("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE         : {:?}", device.preferred_vector_width_double());
        println!("  CL_DEVICE_MAX_CLOCK_FREQUENCY                   : {:?}", device.max_clock_frequency());
        println!("  CL_DEVICE_ADDRESS_BITS                          : {:?}", device.address_bits());
        println!("  CL_DEVICE_MAX_READ_IMAGE_ARGS                   : {:?}", device.max_read_image_args());
        println!("  CL_DEVICE_MAX_WRITE_IMAGE_ARGS                  : {:?}", device.max_write_image_args());
        println!("  CL_DEVICE_MAX_MEM_ALLOC_SIZE                    : {:?}", device.max_mem_alloc_size());
        println!("  CL_DEVICE_IMAGE2D_MAX_WIDTH                     : {:?}", device.image2d_max_width());
        println!("  CL_DEVICE_IMAGE2D_MAX_HEIGHT                    : {:?}", device.image2d_max_height());
        println!("  CL_DEVICE_IMAGE3D_MAX_WIDTH                     : {:?}", device.image3d_max_width());
        println!("  CL_DEVICE_IMAGE3D_MAX_HEIGHT                    : {:?}", device.image3d_max_height());
        println!("  CL_DEVICE_IMAGE3D_MAX_DEPTH                     : {:?}", device.image3d_max_depth());
        println!("  CL_DEVICE_IMAGE_SUPPORT                         : {:?}", device.image_support());
        println!("  CL_DEVICE_MAX_PARAMETER_SIZE                    : {:?}", device.max_parameter_size());
        println!("  CL_DEVICE_MAX_SAMPLERS                          : {:?}", device.max_samplers());
        println!("  CL_DEVICE_MEM_BASE_ADDR_ALIGN                   : {:?}", device.mem_base_addr_align());
        // println!("  CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE              : {:?}", device.min_data_type_align_size());
        println!("  CL_DEVICE_SINGLE_FP_CONFIG                      : {:?}", device.single_fp_config());
        println!("  CL_DEVICE_GLOBAL_MEM_CACHE_TYPE                 : {:?}", device.global_mem_cache_type());
        println!("  CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE             : {:?}", device.global_mem_cacheline_size());
        println!("  CL_DEVICE_GLOBAL_MEM_CACHE_SIZE                 : {:?}", device.global_mem_cache_size());
        println!("  CL_DEVICE_GLOBAL_MEM_SIZE                       : {:?}", device.global_mem_size());
        println!("  CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE              : {:?}", device.max_constant_buffer_size());
        println!("  CL_DEVICE_MAX_CONSTANT_ARGS                     : {:?}", device.max_constant_args());
        println!("  CL_DEVICE_LOCAL_MEM_TYPE                        : {:?}", device.local_mem_type());
        println!("  CL_DEVICE_LOCAL_MEM_SIZE                        : {:?}", device.local_mem_size());
        println!("  CL_DEVICE_ERROR_CORRECTION_SUPPORT              : {:?}", device.error_correction_support());
        println!("  CL_DEVICE_PROFILING_TIMER_RESOLUTION            : {:?}", device.profiling_timer_resolution());
        println!("  CL_DEVICE_ENDIAN_LITTLE                         : {:?}", device.endian_little());
        println!("  CL_DEVICE_AVAILABLE                             : {:?}", device.available());
        println!("  CL_DEVICE_COMPILER_AVAILABLE                    : {:?}", device.compiler_available());
        println!("  CL_DEVICE_EXECUTION_CAPABILITIES                : {:?}", device.execution_capabilities());
        // println!("  CL_DEVICE_QUEUE_PROPERTIES                      : {:?}", device.queue_properties()); // deprecated
        println!("  CL_DEVICE_QUEUE_ON_HOST_PROPERTIES              : {:?}", device.queue_on_host_properties());
        println!("  CL_DEVICE_NAME                                  : {:?}", device.name());
        println!("  CL_DEVICE_VENDOR                                : {:?}", device.vendor());
        println!("  CL_DRIVER_VERSION                               : {:?}", device.driver_version());
        println!("  CL_DEVICE_PROFILE                               : {:?}", device.profile());
        println!("  CL_DEVICE_VERSION                               : {:?}", device.version());
        println!("  CL_DEVICE_EXTENSIONS                            : {:?}", device.extensions());
        println!("  CL_DEVICE_PLATFORM                              : {:?}", device.platform());
        println!("  CL_DEVICE_DOUBLE_FP_CONFIG                      : {:?}", device.double_fp_config());
        // println!("  CL_DEVICE_HALF_FP_CONFIG                        : {:?}", device.half_fp_config());
        println!("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF           : {:?}", device.preferred_vector_width_half());
        // println!("  CL_DEVICE_HOST_UNIFIED_MEMORY                   : {:?}", device.host_unified_memory()); // deprecated
        println!("  CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR              : {:?}", device.native_vector_width_char());
        println!("  CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT             : {:?}", device.native_vector_width_short());
        println!("  CL_DEVICE_NATIVE_VECTOR_WIDTH_INT               : {:?}", device.native_vector_width_int());
        println!("  CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG              : {:?}", device.native_vector_width_long());
        println!("  CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT             : {:?}", device.native_vector_width_float());
        println!("  CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE            : {:?}", device.native_vector_width_double());
        println!("  CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF              : {:?}", device.native_vector_width_half());
        println!("  CL_DEVICE_OPENCL_C_VERSION                      : {:?}", device.opencl_c_version());
        println!("  CL_DEVICE_LINKER_AVAILABLE                      : {:?}", device.linker_available());
        println!("  CL_DEVICE_BUILT_IN_KERNELS                      : {:?}", device.built_in_kernels());
        println!("  CL_DEVICE_IMAGE_MAX_BUFFER_SIZE                 : {:?}", device.image_max_buffer_size());
        println!("  CL_DEVICE_IMAGE_MAX_ARRAY_SIZE                  : {:?}", device.image_max_array_size());
        println!("  CL_DEVICE_PARENT_DEVICE                         : {:?}", device.parent_device());
        println!("  CL_DEVICE_PARTITION_MAX_SUB_DEVICES             : {:?}", device.partition_max_sub_devices());
        // println!("  CL_DEVICE_PARTITION_PROPERTIES                  : {:?}", device.partition_properties());
        println!("  CL_DEVICE_PARTITION_AFFINITY_DOMAIN             : {:?}", device.partition_affinity_domain());
        // println!("  CL_DEVICE_PARTITION_TYPE                        : {:?}", device.partition_type());
        println!("  CL_DEVICE_REFERENCE_COUNT                       : {:?}", device.reference_count());
        println!("  CL_DEVICE_PREFERRED_INTEROP_USER_SYNC           : {:?}", device.preferred_interop_user_sync());
        println!("  CL_DEVICE_PRINTF_BUFFER_SIZE                    : {:?}", device.printf_buffer_size());
        println!("  CL_DEVICE_IMAGE_PITCH_ALIGNMENT                 : {:?}", device.image_pitch_alignment());
        println!("  CL_DEVICE_IMAGE_BASE_ADDRESS_ALIGNMENT          : {:?}", device.image_base_address_alignment());
        println!("  CL_DEVICE_MAX_READ_WRITE_IMAGE_ARGS             : {:?}", device.max_read_write_image_args());
        println!("  CL_DEVICE_MAX_GLOBAL_VARIABLE_SIZE              : {:?}", device.max_global_variable_size());
        println!("  CL_DEVICE_QUEUE_ON_DEVICE_PROPERTIES            : {:?}", device.queue_on_device_properties());
        println!("  CL_DEVICE_QUEUE_ON_DEVICE_PREFERRED_SIZE        : {:?}", device.queue_on_device_preferred_size());
        println!("  CL_DEVICE_QUEUE_ON_DEVICE_MAX_SIZE              : {:?}", device.queue_on_device_max_size());
        println!("  CL_DEVICE_MAX_ON_DEVICE_QUEUES                  : {:?}", device.max_on_device_queues());
        println!("  CL_DEVICE_MAX_ON_DEVICE_EVENTS                  : {:?}", device.max_on_device_events());
        println!("  CL_DEVICE_SVM_CAPABILITIES                      : {:?}", device.svm_capabilities());
        println!("  CL_DEVICE_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE  : {:?}", device.global_variable_preferred_total_size());
        println!("  CL_DEVICE_MAX_PIPE_ARGS                         : {:?}", device.max_pipe_args());
        println!("  CL_DEVICE_PIPE_MAX_ACTIVE_RESERVATIONS          : {:?}", device.pipe_max_active_reservations());
        println!("  CL_DEVICE_PIPE_MAX_PACKET_SIZE                  : {:?}", device.pipe_max_packet_size());
        println!("  CL_DEVICE_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT   : {:?}", device.preferred_platform_atomic_alignment());
        println!("  CL_DEVICE_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT     : {:?}", device.preferred_global_atomic_alignment());
        println!("  CL_DEVICE_PREFERRED_LOCAL_ATOMIC_ALIGNMENT      : {:?}", device.preferred_local_atomic_alignment());
        println!("  CL_DEVICE_IL_VERSION                            : {:?}", device.il_version());
        println!("  CL_DEVICE_MAX_NUM_SUB_GROUPS                    : {:?}", device.max_num_sub_groups());
        // println!("  CL_DEVICE_SUB_GROUP_INDEPENDENT_FORWARD_PROGRESS: {:?}", device.sub_group_independent_forward_progress());
        println!();

        let ctx = Context::with_devices(&[device], &[])?;
        println!("Context: {:?}", ctx);
        println!("  CL_CONTEXT_REFERENCE_COUNT: {:?}", ctx.reference_count());
        println!("  CL_CONTEXT_NUM_DEVICES    : {:?}", ctx.num_devices());
        println!("  CL_CONTEXT_DEVICES        : {:?}", ctx.devices());
        println!();

        let queue = CommandQueue::with_context(&ctx, &[])?;
        println!("CommandQueue: {:?}", queue);
        println!();

        let buf = Buffer::with_size(&ctx, MemFlags::READ_WRITE, 1024)?;
        println!("Buffer: {:?}", buf);
        println!("  CL_MEM_TYPE: {:?}", buf.get_type());
        println!("  CL_MEM_FLAGS: {:?}", buf.flags());
        println!("  CL_MEM_SIZE: {:?}", buf.size());
        println!("  CL_MEM_HOST_PTR: {:?}", buf.host_ptr());
        println!("  CL_MEM_MAP_COUNT: {:?}", buf.map_count());
        println!("  CL_MEM_REFERENCE_COUNT: {:?}", buf.reference_count());
        println!("  CL_MEM_CONTEXT: {:?}", buf.context());
        println!("  CL_MEM_ASSOCIATED_MEMOBJECT: {:?}", buf.associated_memobject());
        println!("  CL_MEM_OFFSET: {:?}", buf.offset());
        println!("  CL_MEM_USES_SVM_POINTER: {:?}", buf.uses_svm_pointer());
        println!();

        // println!("ref_cnt: {:?}", ctx.reference_count());
        // println!("ref_cnt: {:?}", buf.context()?.reference_count());

        Ok(())
    }

    #[test]
    fn kernel_test() {
        kernel_test_().unwrap();
    }

    fn kernel_test_() -> Result<()> {
        let p = Platform::first()?.unwrap();
        let devs = p.devices(DeviceType::All)?;
        let dev = &devs[0];
        let ctx = Context::with_devices(&[dev], &[])?;
        let q = CommandQueue::with_context(&ctx, &[])?;

        let src = r#"
        #include <pzc_builtin.h>

        void pzc_add(const double *a, const double *b, double *c) {
            int gid = get_pid() * 8 + get_tid();
            c[gid] = a[gid] * b[gid];
            flush();
        }
        "#;

        let progn = Program::with_source(&ctx, &[src])?;
        println!("progn: {:?}", progn);
        progn.build(&[dev], "")?;
        let kernel = Kernel::new(&progn, "add")?;
        println!("kernel: {:?}", kernel);

        let add = make_kernel!(&progn, "add", &Buffer, &Buffer, &Buffer);

        println!("Kernel name: {}", kernel.function_name()?);
        println!("Num args: {}", kernel.num_args()?);
        println!("Attributes: {}", kernel.attributes()?);

        println!("Args:");
        for i in 0..kernel.num_args()? {
            println!("  Arg[{}]:", i);
            println!("    Name: {}", kernel.arg_name(i)?);
            println!("    Type: {}", kernel.arg_type_name(i)?);
            println!("    Type Qualifier: {:?}", kernel.arg_type_qualifier(i)?);
            // TODO: disable for avoid backend's bug
            // println!("    Access Qualifier: {:?}", kernel.arg_access_qualifier(i)?);
            println!(
                "    Address Qualifier: {:?}",
                kernel.arg_address_qualifier(i)?
            );
        }

        let gws = 1984 * 8;
        let buf_a = Buffer::with_size(&ctx, MemFlags::READ_WRITE, gws * 8)?;
        let buf_b = Buffer::with_size(&ctx, MemFlags::READ_WRITE, gws * 8)?;
        let buf_c = Buffer::with_size(&ctx, MemFlags::READ_WRITE, gws * 8)?;

        let mut a = vec![0f64; gws];
        let mut b = vec![0f64; gws];

        for i in 0..gws {
            a[i] = (i * 2) as f64;
            b[i] = (i * 2 + 1) as f64;
        }

        q.enqueue_write_buffer(&buf_a, true, 0, gws * 8, &a, &[])?;
        q.enqueue_write_buffer(&buf_b, true, 0, gws * 8, &b, &[])?;

        add(&q, &[gws], &buf_a, &buf_b, &buf_c)?.wait()?;

        let mut c = vec![0f64; gws];
        q.enqueue_read_buffer(&buf_c, true, 0, gws * 8, &mut c, &[])?;

        println!("res: {:?}", c);

        Ok(())
    }
}
